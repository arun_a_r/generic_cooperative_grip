--------------------------------------------------------------------------------
-------------------------Global Variables---------------------------------------
--------------------------------------------------------------------------------
count_time = 0
--------------------------------------------------------------------------------
------------------------------Messages------------------------------------------
--------------------------------------------------------------------------------
call_msg = {1,2,3,4,5,6,7,8,9,10}
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
function init()
    state = "roaming"
    robot.colored_blob_omnidirectional_camera.enable()
    robot.turret.set_position_control_mode()
end
--------------------------------------------------------------------------------
---------------------Inside  Step All States are Defined------------------------
--------------------------------------------------------------------------------
function step()
    log(robot.id,state)
    if state == "roaming" then
        roam()
    elseif state == "choose" then
        robot.leds.set_single_color(13,"green")
        choose()
    elseif state == "approach" then
        robot.leds.set_single_color(13,"green")
        approach()
    elseif state == "grab" then
        robot.leds.set_single_color(13,"green")
        grab()
    elseif state == "call" then
        robot.leds.set_single_color(13,"green")
        call()
    elseif state == "respond" then
        respond()
    end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
function reset()
    state = "roaming"
    robot.colored_blob_omnidirectional_camera.enable()
end
function destroy()
end
--------------------------------------------------------------------------------
---------We avoid obstacles and change state when we find our object------------
--------------------------------------------------------------------------------

function roam()
--------------------------------------------------------------------------------
------------------------Adding respond to roam----------------------------------
--------------------------------------------------------------------------------
    if #robot.range_and_bearing > 0 then
        if robot.range_and_bearing[1].data[2] == 2 then
            robot.wheels.set_velocity(0,0)
            state = "respond"
            int_state = "turn"
        end
    end
--------------------------------------------------------------------------------
    left = robot.proximity[1].value + robot.proximity[2].value +
            robot.proximity[3].value + robot.proximity[4].value +
            robot.proximity[5].value + robot.proximity[6].value
    right = robot.proximity[21].value + robot.proximity[22].value +
            robot.proximity[23].value + robot.proximity[24].value +
            robot.proximity[20].value + robot.proximity[19].value
    if #robot.colored_blob_omnidirectional_camera > 0 then
        gren = 0
        for i = 1, #robot.colored_blob_omnidirectional_camera do
            if robot.colored_blob_omnidirectional_camera[i].color.green > gren then
                gren = robot.colored_blob_omnidirectional_camera[i].color.green
            end
        end
        if gren == 0 then
            state = "choose"
        end
    elseif left ~= 0 then
        robot.wheels.set_velocity(7,-3)
    elseif right ~= 0 then
        robot.wheels.set_velocity(-3,7)
    else
        robot.wheels.set_velocity(10,10)
    end
end
--------------------------------------------------------------------------------
-----------Among the lights we see we choose the nearesr one--------------------
--------------------------------------------------------------------------------
function choose()
    dist = robot.colored_blob_omnidirectional_camera[1].distance
    ang =  robot.colored_blob_omnidirectional_camera[1].angle
    for i = 1, #robot.colored_blob_omnidirectional_camera do
        if dist > robot.colored_blob_omnidirectional_camera[i].distance then
            dist = robot.colored_blob_omnidirectional_camera[i].distance
            ang = robot.colored_blob_omnidirectional_camera[i].angle
        end
    end
    if ang > 0.1 then
        robot.wheels.set_velocity(-1,1)
    elseif ang < -0.1 then
        robot.wheels.set_velocity(1,-1)
    elseif ang >= -0.1 and ang <= 0.1 then
        state = "approach"
    end
end

--------------------------------------------------------------------------------
-----------------------Go capture the chosen box--------------------------------
--------------------------------------------------------------------------------
function approach()
    x = 0
    for i = 1, 24 do
        if x < robot.proximity[i].value then
            x = robot.proximity[i].value
        end
    end
    robot.wheels.set_velocity((1 - x) * 10, (1 - x) * 10)

    if x >= 0.9 then
        robot.wheels.set_velocity(0,0)
        state = "grab"
    end
end

--------------------------------------------------------------------------------
------------------Grab the box you have come near to----------------------------
--------------------------------------------------------------------------------
function grab()
    grip_ang = 200
    for i = 1,24 do
        if robot.proximity[i].value >= 0.9 then
            grip_ang = robot.proximity[i].angle
            break
        end
    end
    if grip_ang == 200 then
        robot.wheels.set_velocity(5,5)
    else
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        count_time = count_time + 1
    end
    if count_time == 100 then
        robot.gripper.lock_positive()
        robot.turret.set_passive_mode()
        count_time = 0
        state = "call"
    end
end

--------------------------------------------------------------------------------
----------------------Calling another robot for help----------------------------
--------------------------------------------------------------------------------
function call()
    robot.wheels.set_velocity(0, 0)
    robot.range_and_bearing.set_data(call_msg)
end

--------------------------------------------------------------------------------
----------------------------Respond to Message----------------------------------
----------------Will use internal states to go to called robot------------------
--------------------------------------------------------------------------------
function respond()
    log(int_state)
    if int_state == "turn" then
        turn()
    elseif int_state == "lunge" then
        lunge()
    elseif int_state == "grab box" then
        navigate()
    end
end
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--------------------------Turn internal function--------------------------------
--------------------------------------------------------------------------------
function turn()
    if #robot.range_and_bearing == 0 then
        robot.wheels.set_velocity(0,0)
    elseif #robot.range_and_bearing > 0 then
        if robot.range_and_bearing[1].horizontal_bearing < 0.05 and
            robot.range_and_bearing[1].horizontal_bearing > -0.05 then
                int_state = "lunge"
        elseif robot.range_and_bearing[1].horizontal_bearing > 0 then
            robot.wheels.set_velocity(-0.25,0.25)
        elseif robot.range_and_bearing[1].horizontal_bearing < 0 then
            robot.wheels.set_velocity(0.25,-0.25)
        end
    end
end
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
----------------------Lunge toward the called robot-----------------------------
--------------------------------------------------------------------------------
function lunge()
    x = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x then
            x = robot.proximity[i].value
        end
    end
    robot.wheels.set_velocity((1 - x) * 10,(1 - x) * 10)
    if x >= 0.9 then
        robot.wheels.set_velocity(0,0)
    end
end
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
