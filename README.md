# Generic Grip

## This project is aimed to be something like a co-operative foraging

# Aims
1. Two types of boxes green & red
2. Red can be moved only by two robots
3. Green one can be moved by one robots
4. Aim is to use a swarm of robots to gather these boxes to a desired location

# Success
1. Implemented the red box with lights so that robots can see
2. Can distribute the robots with different orientation
3. Robot can see the boxes and go grab them
4. Robot can call another robot for help

# To Succeed
1. Need to synchronize both robots to pull the box
2. Must stop more than one robot from from coming to the same robot
